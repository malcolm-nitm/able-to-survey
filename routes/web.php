<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [
'middleware' => ['auth'],
'uses' => 'HomeController@index'
], function () {
    return view('home');
})->name('home');

Route::post('/answer', [
'middleware' => ['auth'],
'uses' => 'HomeController@answer'
])->name('answer');

Route::get('/results', [
'middleware' => ['auth'],
'uses' => 'HomeController@results'
])->name('results');

Auth::routes();

/**
* Admin resources
*/
Route::group(['prefix' => 'admin', 'middleware' => ['auth', 'admin']], function () {
    Route::get('/', 'Admin\IndexController@index')->name('admin');
    
    Route::resources([
    'surveys' => 'Admin\SurveyController',
    'questions' => 'Admin\QuestionController',
    'answers' => 'Admin\AnswerController',
    'choices' => 'Admin\ChoiceController',
    ]);
});