# Laravel Framework 5.5 Daily Survey Tool

## Starter Site based on on Laravel 5.5
* [Requirements](#feature2)
* [How to install](#feature3)

-----
<a name="feature2"></a>
## Requirements
	PHP >= 7.0.0
    Mysql or Sqlite (For Development)
-----
<a name="feature3"></a>
## Installation:
* [Step 1: Get the code](#step1)
* [Step 2: Use Composer to install dependencies](#step2)
* [Step 3: Create database](#step3)
* [Step 4: Install](#step4)
* [Step 5: Start Page](#step5)

-----
<a name="step1"></a>
### Step 1: Clone Repository
   git clone git@gitlab.com:malcolm-nitm/able-to-survey.git
   cd able-to-survey
-----
<a name="step2"></a>
### Step 2: Use Composer to install dependencies

Laravel utilizes [Composer](http://getcomposer.org/) to manage its dependencies. First, download a copy of the composer.phar.
Once you have the PHAR archive, you can either keep it in your local project directory or move to
usr/local/bin to use it globally on your system.
On Windows, you can use the Composer [Windows installer](https://getcomposer.org/Composer-Setup.exe).

Then run:

    composer dump-autoload
    composer install --no-scripts

-----
<a name="step3"></a>
### Step 1: Set the environment, run migrations and seed the database

    *If using sqlite then create db.sqlite in the dataabse folder*

    cp .env.production .env
    php artisan migrate
    php artisan db:seed
-----
<a name="step4"></a>
### Step 4: Run the App
    php artisan serve
-----
<a name="step5"></a>
### Step 5: View App

Visit the app by opening your web browser to:

    http://localhost:8000

You can now login to admin:

    username: admin@app.tld
    password: admin

OR create your own user by registering

-----
