<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnswersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('answers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('survey_id');
            $table->integer('question_id');
            $table->integer('choice_id');
            $table->integer('user_id');
            $table->timestamps();
        });
        
        Schema::table('answers', function(Blueprint $table) {
            // $table->foreign('survey_id')->references('id')->on('surveys')->onDelete('cascade');
            // $table->foreign('question_id')->references('id')->on('questions')->onDelete('cascade');
            // $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            // $table->unique(['user_id', 'question_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('answers', function(Blueprint $table) {
            $table->dropForeign(['question_id']);
            $table->dropForeign(['survey_id']);
            $table->dropForeign(['user_id']);
        });
        Schema::dropIfExists('answers');
    }
}
