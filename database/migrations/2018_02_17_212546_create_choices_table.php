<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('choices', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('survey_id');
            $table->integer('question_id');
            $table->string('value', 255);
            $table->timestamps();
        });
        
        Schema::table('questions', function(Blueprint $table) {
            // $table->foreign('survey_id')->references('id')->on('surveys')->onDelete('cascade');
            // $table->foreign('question_id')->references('id')->on('questions')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('choices', function(Blueprint $table) {
            $table->dropForeign(['question_id']);
            $table->dropForeign(['survey_id']);
        });
        Schema::dropIfExists('choices');
    }
}
