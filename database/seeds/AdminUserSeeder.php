<?php

use Illuminate\Database\Seeder;
use App\User;
use Illuminate\Support\Facades\Hash;

class AdminUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = User::firstOrNew(['email' => 'admin@app.tld']);
        if(!$admin->exists) {
            $admin = User::create([
                'email' => 'admin@app.tld',
                'name' => 'Admin User',
                'is_admin' => true,
                'remember_token' => str_random(10),
                'password' => Hash::make('admin')
            ]);
        }
        $user = \Auth::setUser($admin);
    }
}
