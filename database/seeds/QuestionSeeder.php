<?php

use Illuminate\Database\Seeder;

use \App\Survey;
use \App\User;

class QuestionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $survey = Survey::firstOrCreate([
            'title' => 'Daily checkups',
            'description' => "We'll ask a fee question to see how your day is going",
            'user_id' => User::first()->id,
            'is_active' => 1
        ]);
        foreach($this->sampleData()['questions'] as $data) {
            $data['is_active'] = 1;
            $question = $survey->questions()->create(array_only($data, ['question', 'day_of_week', 'is_active']));
            foreach($data['choices'] as $choice) {
                $choice['survey_id'] = $survey->id;
                $question->choices()->create($choice);
            }
        }
    }

    protected function sampleData() {
        return [
            'questions' => [
                [
                    'question' => "How's your day going so far?",
                    'day_of_week' => 'Mon',
                    'choices' => [
                        [
                            "value" => "Great!"
                        ], [
                            'value' => "Ok"
                        ], [
                            'value' => "Not Bad"
                        ], [
                            'value' => "Horrible"
                        ]
                    ]
                ], [
                    'question' => "What are you working on today?",
                    'day_of_week' => 'Tue',
                    'choices' => [
                        [
                            "value" => "Interview assignments!"
                        ], [
                            'value' => "Reviewing interview assignments"
                        ], [
                            'value' => "Working on work sir"
                        ], [
                            'value' => "Thinking about yesterday :-/"
                        ]
                    ]
                ], [
                    'question' => "What do you plan to do after work?",
                    'day_of_week' => 'Wed',
                    'choices' => [
                        [
                            "value" => "Sleep!"
                        ], [
                            'value' => "Eat"
                        ], [
                            'value' => "Game"
                        ], [
                            'value' => "Excercise"
                        ]
                    ]
                ], [
                    'question' => "Did you excersice today?",
                    'day_of_week' => 'Thu',
                    'choices' => [
                        [
                            "value" => "Yes!"
                        ], [
                            'value' => "No"
                        ], [
                            'value' => "Not Yet"
                        ], [
                            'value' => "I won't Excercist Today"
                        ]
                    ]
                ], [
                    'question' => "How do you feel about surveys?",
                    'day_of_week' => 'Fri',
                    'choices' => [
                        [
                            "value" => "They're the best!"
                        ], [
                            'value' => "Surveys can be fun"
                        ], [
                            'value' => "They're distracting"
                        ], [
                            'value' => "Umm..."
                        ]
                    ]
                ], [
                    'question' => "What are you doing today?",
                    'day_of_week' => 'Sat',
                    'choices' => [
                        [
                            "value" => "Brunch and chill!"
                        ], [
                            'value' => "Netflix and chill!"
                        ], [
                            'value' => "Netflix"
                        ], [
                            'value' => "Chill"
                        ]
                    ]
                ], [
                    'question' => "Ready for the work week?",
                    'day_of_week' => 'Sun',
                    'choices' => [
                        [
                            "value" => "I'm pumped!"
                        ], [
                            'value' => "I have tasks to accomplish"
                        ], [
                            'value' => "The weekend is over already?"
                        ], [
                            'value' => "What year is it?."
                        ]
                    ]
                ]
            ]
        ];
    }
}
