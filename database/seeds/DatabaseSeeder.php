<?php

use Illuminate\Database\Seeder;
// use AdminUserSeeder;
// use QuestionSeeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(AdminUserSeeder::class);
        $this->call(QuestionSeeder::class);
    }
}
