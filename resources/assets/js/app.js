
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('example-component', require('./components/ExampleComponent.vue'));

const app = new Vue({
    el: '#app'
});

//Setup dynamic input fields
function setupDependsOn() {
    $('[data-depends-on]').on('change', function() {
        let $this = $(this);
        let data = $this.data('data') || {};
        let $target = $($this.data('target'));
        if ($this.val()) {
            $.ajax({
                url: $this.data('url'),
                type: "GET",
                dataType: "json",
                data: $.extend(true, data, {depends: {value: $this.val()}}),
                beforeSend: function() {
                    $('#loader').css("visibility", "visible");
                },
   
                success: function(data) { 
                    $target.empty();
                    $target.removeAttr('disabled');
                    $target.append('<option value="">Select value...</option>');
                    $.each(data, function(key, value) {
   
                        $target.append('<option value="' + key + '">' + value + '</option>');
   
                    });
                },
                complete: function() {
                    $('#loader').css("visibility", "hidden");
                }
            });
        } else {
            $target.empty();
        }
   
    });
}

//Setup ajax forms
function setupAjaxForms($container) {
    $container = $container || $('body');
    $container.find('[data-ajax-form]').on('submit',function(e){
        let $form = $(this);
        let formData = $form.serialize();
        e.preventDefault(e);
        $.ajax({
            type: "POST",
            url: $form.attr('action'),
            data: formData,
            dataType: 'json',
            beforeSend: function () {
                //Disable some inputs...etc. before sending request
                $form.find(':input,select,textarea').addClass('disabled').attr('disabled', true);
                let $submit = $form.find(':submit');
                $form.find( '#form-errors' ).html('');
                $submit.data('btn-text', $submit.text()).text('Working...');
            },
            success: (data) => {
                let afterFunc = eval($form.data('after') || null);
                if(typeof afterFunc == 'function') {
                    afterFunc(data);
                } else if(data.html) {
                    let $target = $($form.data('success-target')) || $form;
                    $target.slideUp();
                    if($form.data('replace-target') !== false) {
                        $target.html(data.html);
                    } else {
                        $target.append(data.html);
                    }
                    $target.slideDown();
                    let initFunc = eval($form.data('init') || null);
                    if(typeof initFunc == 'function') {
                        initFunc(null, $target);
                    }

                    //Need to reinit/setup ajax forms here
                    setupAjaxForms($target);
                }

                if(data.message) {
                    let $alert = $('.alert');
                    if($alert.length) {
                        $alert.first().text(data.message).addClass('alert-success');
                        setTimeout(function () {
                            $alert.hide('slide-up');
                        }, 10000);
                    }
                }
                if(!$form.data('no-reset')) {
                    $form.get(0).reset();
                }
            },
            error: (jqXhr) => {
                if( jqXhr.status === 401 ) //redirect if not authenticated user.
                $( location ).prop( 'pathname', 'login' );
                if( jqXhr.status === 422 ) {
                    //process validation errors here.
                    $errors = jqXhr.responseJSON; //this will get the errors response data.
                    //show them somewhere in the markup
                    //e.g

                    let globalErrors = '';
            
                    $.each( $errors.errors, function( key, value ) {
                        // $input = $form.find("[name="+key+"], #"+key);
                        // if($input.length) {
                        // } else {
                            globalErrors += '<li>' + value[0] + '</li>'; //showing only the first error.
                        // }
                    });
                    //If we have global errors show it
                    if(globalErrors.length) {
                        errorsHtml = '<div class="alert alert-danger"><ul>'+globalErrors+'</ul></di>';
                        $form.find( '#form-errors' ).html( errorsHtml ); //appending to a <div id="form-errors"></div> inside form
                    }
                } else {
                    /// do some thing else
                    $form.find( '#form-errors' ).html( '<div class="alert alert-danger"><ul>'+jqXhr.responseJSON.message+'</ul></di>' ); //appending to a <div id="form-errors"></div> inside form
                }
            }
        }).always(() => {
            //Always re-nable inputs
            let $submit = $form.find(':submit');
            $submit.text($submit.data('btn-text'));
            $form.find(':input,select,textarea').removeClass('disabled').removeAttr('disabled');
        })
    });
}

/** 
 * Setup delete functionality
*/
function setupDeleteActions() {

    $('[data-ajax-delete]').on('click', function (e) {
        let $this = $(this);
        if(confirm($this.data('confirm') || 'Are you sure you want to delete this?')){
            $.ajax({
                url: $this.attr('href') || $this.data('url'),
                type: 'DELETE',
                data: $this.data('data'),
                success: function(data) {
                    $($this.data('target')).slideUp();
                },
                error: function (jqXhr) {
                    let oldText = $this.text();
                    $this.text(jqXhr.responseJSON.message || jqXhr.responseText);
                    setTimeout(function () {
                        $this.text(oldText);
                    })
                }
            });
        }
    });
}

$(function(){
    //Setup global ajax parameters
    $.ajaxSetup({
        header: $('meta[name="_token"]').attr('content'),
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    //For drop down dependent information
    setupDependsOn();

    //Setup delete actions
    setupDeleteActions();

    //Setup dynamic forms
    setupAjaxForms();
});
