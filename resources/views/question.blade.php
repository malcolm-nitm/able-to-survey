<h2>{{$question->title}}</h2>
{!! Form::model(new \App\Answer([
    'question_id' => $question->id,
    'survey_id' => $question->survey_id
]), [
    'method' => 'POST',
    'class' => 'form',
    'data-ajax-form' => true,
    'data-success-target' => '#question',
    'data-replace-target' => true,
    'data-no-reset' => true,
    'data-init' => 'initQuestion',
    'data-id' => $question->id,
    'route' => [
        'answer'
    ]
]) !!}
<div class="list-group">
    @foreach($question->choices as $choice)
    {!! Form::radio('choice_id', $choice->id, false, [
        'id' => 'choice'.$choice->id,
        'class' => 'hidden'
    ]) !!}
    {!! Form::label('choice'.$choice->id, $choice->value, [
        'style' => 'width: 100%; padding: 10px; cursor: pointer'
    ]) !!}
    @endforeach
</div>
<input type="hidden" name="question_id" value="{{$question->id}}" />
<input type="hidden" name="survey_id" value="{{$question->survey_id}}" />
@if (isset($next) && $next !== null)
{!! Form::submit('Next', ['class' => 'btn btn-success', 'id' => 'submit', 'disabled' => true, 'style' => 'width: 100%']) !!}
@else
{!! Form::submit('Save', ['class' => 'btn btn-success', 'id' => 'submit', 'disabled' => true, 'style' => 'width: 100%']) !!}
@endif
{!! Form::close() !!}

<script>
/**
 * Initial handling of selected qustions
*/
function initQuestion(event, $container) {
    $container = $container !== undefined ? $container : $('#question');
    $container.find(':radio').on('change', function (e) {
        $container.find('.list-group-item-success').removeClass('list-group-item-success');
        $('#submit').removeAttr('disabled'); 
        //Change background color based on whether checkbox is checked
        $("label[for='"+$(this).attr('id')+"']").toggleClass('list-group-item-success', this.checked)
    });
}
document.addEventListener('DOMContentLoaded', initQuestion);
</script>