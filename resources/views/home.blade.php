@extends('layouts.app') @section('content')
<div class="row">
  <div class="col-sm-12">
    <div class="panel panel-default">
      <div class="panel-heading">Dashboard</div>

      <div class="panel-body">
      @if (isset($question))
        <h1>Hey {{\Auth::user()->name}}!</h1>
        <p class="info">Would you mind answering the question below?</p>
        <div id='question'>
          @component('question', [
            'question' => $question,
            'next' => $next ?: null
          ])
          @endcomponent
        </div>
      @else
        <h2>Sorry no surveys for today! Check back later</h2>
      @endif
      </div>
    </div>
  </div>
</div>
@endsection