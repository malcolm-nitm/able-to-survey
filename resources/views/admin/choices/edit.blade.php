@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="pull-left">
                <h2>Edit Answer: {{ $model->title}} </h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('answers.index') }}"> Back</a>
            </div>
        </div>
    </div>

    @if ($errors->any())
        @component('alert', ['errors' => $errors])
        @endcomponent
    @endif

    {!! Form::model($model, [
        'method' => 'PATCH',
        'route' => [
            'answers.update', $model->id
        ]
    ]) !!}
        @include('admin.answers.form')
    {!! Form::close() !!}
@endsection

