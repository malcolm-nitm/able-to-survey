@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2> Show Answer</h2>
        </div>
        <div class="pull-right">
            <a class="btn btn-primary" href="{{ route('answers.index') }}"> Back</a>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Survey:</strong>
            {{ $model->survey->title}}
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Question:</strong>
            {{ $model->question->question }}
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Answer:</strong>
            {{ $model->value }}
        </div>
    </div>
</div>
@endsection

