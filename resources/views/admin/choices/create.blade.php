@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="pull-left">
                <h2>Add New Choice</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('choices.index') }}"> Back</a>
            </div>
        </div>
    </div>

    @if ($errors->any())
        @component('alert', ['errors' => $errors])
        @endcomponent
    @endif

    {!! Form::open([
        'route' => 'choices.store',
        'method'=>'POST'
    ]) !!}
         @include('admin.choices.form')
    {!! Form::close() !!}
@endsection

