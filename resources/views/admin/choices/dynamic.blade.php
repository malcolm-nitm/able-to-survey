@if ($model && $model->exists)
<div class="row" id="choice-{{ $model->id }}">
{!! Form::model($model, [
    'method' => 'PATCH',
    'class' => 'form',
    'data-ajax-form' => true,
    'route' => [
        'choices.update', $model->id
    ]
]) !!}
<div class="col-sm-12">
    <div id="form-errors"></div>
</div>
<div class="col-xs-12 col-sm-12 col-md-12">
    <div class="form-group">
        {!! Form::text('value', null, ['class' => 'form-control', 'placeholder' => 'Enter Choice']) !!}
    </div>
</div>
<div class="col-xs-12 col-sm-12 col-md-12">
    <div class="form-group">
    {!! Form::submit('Save', ['class' => 'btn btn-primary col-sm-12 col-md-6']) !!}
    {!! Form::button('Delete', [
        'class' => 'btn btn-danger col-sm-12 col-md-6', 
        'data-ajax-delete' => true,
        'data-url' => route('choices.destroy', $model->id),
        'data-target' => '#choice-'.$model->id,
        'data-confirm' => 'Are you sure you want to delete this choice?'
    ]) !!}
    {!! Form::hidden('question_id') !!}
    </div>
</div>
{!! Form::close() !!}
</div>
@endif

@if (isset($isTemplate) && $isTemplate)
<div class="section well">
    <h2>Add New Choice to: {{ $question->question }}</h2>  
    {!! Form::model($model, [
        'method' => 'POST',
        'class' => 'form',
        'data-ajax-form' => true,
        'data-success-target' => '#choices',
        'route' => [
            'choices.store', $question->id
        ]
    ]) !!}
    <div class="row">
        <div class="col-sm-12">
            <div id="form-errors"></div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                {!! Form::text('value', null, ['class' => 'form-control', 'placeholder' => 'Enter Choice']) !!}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
            {!! Form::submit('Add', ['class' => 'btn btn-success', 'style' => 'width: 100%']) !!}
            {!! Form::hidden('question_id') !!}
            {!! Form::hidden('survey_id') !!}
            </div>
        </div>
    </div>
    {!! Form::close() !!}
</div>
@endif

<div class="section container" id="choices">
@if (isset($choices) && $choices->count())
    <h2>Choices</h2>  
        @foreach ($choices as $choice)
        <div class="row" id="choice-{{ $choice->id }}">
            {!! Form::model($choice, [
                'method' => 'PATCH',
                'class' => 'form',
                'data-ajax-form' => true,
                'route' => [
                    'choices.update', $choice->id
                ]
            ]) !!}
            <div class="col-sm-12">
                <div id="form-errors"></div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    {!! Form::text('value', null, ['class' => 'form-control', 'placeholder' => 'Enter Choice']) !!}
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                {!! Form::submit('Save', ['class' => 'btn btn-primary col-sm-12 col-md-6']) !!}
                {!! Form::button('Delete', [
                    'class' => 'btn btn-danger col-sm-12 col-md-6', 
                    'data-ajax-delete' => true,
                    'data-url' => route('choices.destroy', $choice->id),
                    'data-target' => '#choice-'.$choice->id,
                    'data-confirm' => 'Are you sure you want to delete this choice?'
                ]) !!}
                </div>
            </div>
            <br>
            {!! Form::close() !!}
        </div>
        <br>
        @endforeach
@endif
</div>