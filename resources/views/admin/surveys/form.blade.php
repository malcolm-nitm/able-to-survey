<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            {!! Form::label('title', 'Title') !!}
            {!! Form::text('title', null, array('placeholder' => 'Title','class' => 'form-control')) !!}
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            {!! Form::label('description', 'Description') !!}
            {!! Form::textarea('description', null, array('placeholder' => 'Body','class' => 'form-control','style'=>'height:150px')) !!}
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            {!! Form::label('status-draft', 'Draft') !!}
            {!! Form::radio('status', 'ready', ($model->status == 'draft'), [
            'id' => 'status-draft']) !!}
            {!! Form::label('status-ready', 'Ready') !!}
            {!! Form::radio('status', 'ready', ($model->status == 'ready'), [
            'id' => 'status-ready']) !!}
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 text-center">
        {!! Form::submit('Save', [
        'class' => 'btn btn-primary'
        ]) !!}
    </div>
</div>