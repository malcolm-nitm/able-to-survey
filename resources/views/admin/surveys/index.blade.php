@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-right">
                <a class="btn btn-success" href="{{ route('surveys.create') }}"> Create New Survey</a>
            </div>
        </div>
    </div>
    <br>
    @if ($models->count())
    <table class="table table-bordered">
        <tr>
            <th class="col-sm-2 col-md-1">No</th>
            <th>Title</th>
            <th>Author</th>
            <th>Questions</th>
            <th width="280px">Action</th>

        </tr>
        @foreach ($models as $model)
        <tr>
            <td>{{ ++$i }}</td>
            <td>{{ $model->title}}</td>
            <td>{{ $model->user->name }}</td>
            <td>{{ @$model->questionCount->first()->count ?: 0 }}</td>
            <td>
                <a class="btn btn-info" href="{{ route('surveys.show', $model->id) }}">Show</a>
                <a class="btn btn-primary" href="{{ route('surveys.edit', $model->id) }}">Edit</a>
                {!! Form::open(['method' => 'DELETE','route' => ['surveys.destroy', $model->id],'style'=>'display:inline']) !!}
                    {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
                {!! Form::close() !!}
            </td>
        </tr>
        <tr>
            <td colspan="3">
                {{ $model->description }}
            </td>
        </tr>
        @endforeach
        </table>
    @else
        <div class="alert alert-warning">No surveys to show</div>
    @endif
    {!! $models->links() !!}
@endsection

