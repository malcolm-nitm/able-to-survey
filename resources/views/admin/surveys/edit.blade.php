@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="pull-left">
                <h2>Edit Survey: {{ $model->title}} </h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('surveys.index') }}"> Back</a>
            </div>
        </div>
    </div>

    @if ($errors->any())
        @component('alert', ['errors' => $errors])
        @endcomponent
    @endif

    {!! Form::model($model, [
        'method' => 'PATCH',
        'route' => [
            'surveys.update', $model->id
        ]
    ]) !!}
        @include('admin.surveys.form')
    {!! Form::close() !!}
    @if ($model->exists)
        <br>
        @component('admin.questions.dynamic', [
            'model' => new \App\Question(['survey_id' => $model->id]), 
            'questions' => $model->questions, 
            'isTemplate' => true,
            'survey' => $model
        ])
        @endcomponent
    @endif
@endsection

