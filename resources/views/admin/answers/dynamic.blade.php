@if ($model->exists)
{!! Form::model($model, [
    'method' => 'PATCH',
    'class' => 'form form-inline',
    'route' => [
        'surveys.update-question', $model->id
    ]
]) !!}
{!! Form::submit('Save', ['class' => 'btn' => 'btn-primary']) !!}
{!! Form::close() !!}
@endif

@if (isset($isTemplste) && $isTemplate)
{!! Form::model($model, [
    'method' => 'POST',
    'class' => 'form form-inline',
    'route' => [
        'surveys.add-question', $model->id
    ]
]) !!}
{!! Form::submit('Add', ['class' => 'btn' => 'btn-success']) !!}
{!! Form::close() !!}
@endif