@extends('layouts.app')

@section('content')
<div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-right">
                <a class="btn btn-success" href="{{ route('answers.create') }}"> Create New Answer</a>
            </div>
        </div>
    </div>
    <br>
    @if ($models->count())
    <table class="table table-bordered">
        <tr>
            <th class="col-sm-2 col-md-1">No</th>
            <th>Question</th>
            <th>Survey</th>
            <th>Answer</th>
            <th>User</th>
            <th width="280px">Action</th>

        </tr>
        @foreach ($models as $model)
        <tr>
            <td>{{ ++$i }}</td>
            <td>{{ $model->question->question }}</td>
            <td>{{ $model->survey->title }}</td>
            <td>{{ $model->choice->value }}</td>
            <td>{{ $model->user->name }}</td>
            <td>
                <a class="btn btn-info" href="{{ route('answers.show', $model->id) }}">Show</a>
                <a class="btn btn-primary" href="{{ route('answers.edit', $model->id) }}">Edit</a>
                {!! Form::open(['method' => 'DELETE','route' => ['answers.destroy', $model->id],'style'=>'display:inline']) !!}
                    {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
                {!! Form::close() !!}
            </td>
        </tr>
        @endforeach
        </table>
    @else
        <div class="alert alert-warning">No answers to show</div>
    @endif
    {!! $models->links() !!}
@endsection

