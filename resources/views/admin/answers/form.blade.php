<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            {!! Form::select('user_id', \App\User::get()->pluck('name', 'id'), null, [
                'class' => 'form-control', 
                'placeholder' => 'Select User',
                'data-target' => '#survey_id',
                'data-url' => '/admin/surveys',
                'data-data' => '{"depends": {"key": "user_id"}}',
                'data-depends-on' => true
            ]) !!}
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            {!! Form::select('survey_id', [], null, [
                'class' => 'form-control', 
                'placeholder' => 'Select Survey', 
                'disabled' => true,
                'id' => 'survey_id',
                'data-target' => '#question_id',
                'data-data' => '{"depends": {"key": "survey_id"}}',
                'data-url' => '/admin/questions',
                'data-depends-on' => true
            ]) !!}
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            {!! Form::select('question_id', [], null, [
                'class' => 'form-control',
                'placeholder' => 'Select Question', 
                'disabled' => true,
                'id' => 'question_id',
                'data-target' => '#choice_id',
                'data-data' => '{"depends": {"key": "question_id"}}',
                'data-url' => '/admin/choices',
                'data-depends-on' => true
            ]) !!}
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            {!! Form::select('choice_id', [], null, [
                'class' => 'form-control',
                'placeholder' => 'Select Choice', 
                'disabled' => true,
                'id' => 'choice_id'
            ]) !!}
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            {!! Form::text('value', null, ['class' => 'form-control', 'placeholder' => 'Enter Answer']) !!}
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 text-center">
        {!! Form::submit('Save', [
        'class' => 'btn btn-primary'
        ]) !!}
    </div>
</div>