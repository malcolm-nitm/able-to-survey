<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            {!! Form::select('survey_id', \App\Survey::get()->pluck('title', 'id'), null, ['class' => 'form-control', 'placeholder' => 'Select Survey']) !!}
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            {!! Form::text('question', null, ['class' => 'form-control', 'placeholder' => 'Enter Question']) !!}
        </div>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-6">
        <div class="form-group">
        {!! Form::select('day_of_week', \App\Question::daysOptions(), null, ['class' => 'form-control', 'placeholder' => 'Select Day...']) !!}
        </div>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-6">
        <div class="form-group">
        {!! Form::label('is_active') !!}
        {!! Form::checkbox('is_active', true, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 text-center">
        {!! Form::submit('Save', [
        'class' => 'btn btn-primary'
        ]) !!}
    </div>
</div>