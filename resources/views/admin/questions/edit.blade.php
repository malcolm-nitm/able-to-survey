@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="pull-left">
                <h2>Edit Question: {{ $model->title}} </h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('questions.index') }}"> Back</a>
            </div>
        </div>
    </div>

    @if ($errors->any())
        @component('alert', ['errors' => $errors])
        @endcomponent
    @endif

    {!! Form::model($model, [
        'method' => 'PATCH',
        'route' => [
            'questions.update', $model->id
        ]
    ]) !!}
        @include('admin.questions.form')
    {!! Form::close() !!}
    @if ($model->exists)
        <br>
        @component('admin.choices.dynamic', [
            'model' => new \App\Choice([
                'question_id' => $model->id,
                'survey_id' => $model->survey_id
            ]), 
            'choices' => $model->choices, 
            'isTemplate' => true,
            'question' => $model
        ])
        @endcomponent
    @endif
@endsection

