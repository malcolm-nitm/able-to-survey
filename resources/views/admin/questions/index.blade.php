@extends('layouts.app')

@section('content')
<div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-right">
                <a class="btn btn-success" href="{{ route('questions.create') }}"> Create New Question</a>
            </div>
        </div>
    </div>
    <br>
    @if ($models->count())
    <table class="table table-bordered">
        <tr>
            <th class="col-sm-2 col-md-1">No</th>
            <th>Title</th>
            <th>Question</th>
            <th>Day</th>
            <th>Survey</th>
            <th>Choices</th>
            <th>Answers</th>
            <th width="280px">Action</th>

        </tr>
        @foreach ($models as $model)
        <tr>
            <td>{{ ++$i }}</td>
            <td>{{ $model->title}}</td>
            <td>{{ $model->question }}</td>
            <td>{{ $model->day_of_week }}</td>
            <td>{{ $model->survey->title }}</td>
            <td>{{ @$model->choiceCount->first()->count ?: 0 }}</td>
            <td>{{ @$model->answerCount->first()->count ?: 0 }}</td>
            <td>
                <a class="btn btn-info" href="{{ route('questions.show', $model->id) }}">Show</a>
                <a class="btn btn-primary" href="{{ route('questions.edit', $model->id) }}">Edit</a>
                {!! Form::open(['method' => 'DELETE','route' => ['questions.destroy', $model->id],'style'=>'display:inline']) !!}
                    {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
                {!! Form::close() !!}
            </td>
        </tr>
        @endforeach
        </table>
    @else
        <div class="alert alert-warning">No questions to show</div>
    @endif
    {!! $models->links() !!}
@endsection

