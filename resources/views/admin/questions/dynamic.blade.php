@if ($model && $model->exists)
<div class="row" id="question-{{ $model->id }}">
{!! Form::model($model, [
    'method' => 'PATCH',
    'class' => 'form',
    'data-ajax-form' => true,
    'route' => [
        'questions.update', $model->id
    ]
]) !!}
<div class="col-sm-12">
    <div id="form-errors"></div>
</div>
<div class="col-xs-12 col-sm-12 col-md-12">
    <div class="form-group">
        {!! Form::text('question', null, ['class' => 'form-control', 'placeholder' => 'Enter Question']) !!}
    </div>
</div>
<div class="col-xs-12 col-sm-6 col-md-6">
    <div class="form-group">
    {!! Form::select('day_of_week', \App\Question::daysOptions(), null, ['class' => 'form-control', 'placeholder' => 'Select Day...']) !!}
    </div>
</div>
<div class="col-xs-12 col-sm-6 col-md-6">
    <div class="form-group">
    {!! Form::label('is_active') !!}
    {!! Form::checkbox('is_active', true, ['class' => 'form-control']) !!}
    </div>
</div>
<div class="col-xs-12 col-sm-12 col-md-12">
    <div class="form-group">
    {!! Form::submit('Save', ['class' => 'btn btn-primary col-sm-12 col-md-6']) !!}
    {!! Form::button('Delete', [
        'class' => 'btn btn-danger col-sm-12 col-md-6', 
        'data-ajax-delete' => true,
        'data-url' => route('questions.destroy', $model->id),
        'data-target' => '#question-'.$model->id,
        'data-confirm' => 'Are you sure you want to delete this question?'
    ]) !!}
    {!! Form::hidden('survey_id') !!}
    </div>
</div>
{!! Form::close() !!}
</div>
@endif

@if (isset($isTemplate) && $isTemplate)
<div class="section well">
    <h2>Add New Question to: {{ $survey->title }}</h2>  
    {!! Form::model($model, [
        'method' => 'POST',
        'class' => 'form',
        'data-ajax-form' => true,
        'data-success-target' => '#questions',
        'route' => [
            'questions.store', $survey->id
        ]
    ]) !!}
    <div class="row">
        <div class="col-sm-12">
            <div id="form-errors"></div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                {!! Form::text('question', null, ['class' => 'form-control', 'placeholder' => 'Enter Question']) !!}
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-6">
            <div class="form-group">
            {!! Form::select('day_of_week', \App\Question::daysOptions(), null, ['class' => 'form-control', 'placeholder' => 'Select Day...']) !!}
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-6">
            <div class="form-group">
            {!! Form::label('is_active') !!}
            {!! Form::checkbox('is_active', true, ['class' => 'form-control']) !!}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
            {!! Form::submit('Add', ['class' => 'btn btn-success', 'style' => 'width: 100%']) !!}
            {!! Form::hidden('survey_id') !!}
            </div>
        </div>
    </div>
    {!! Form::close() !!}
</div>
@endif

<div class="section container" id="questions">
@if (isset($questions) && $questions->count())
    <h2>Questions</h2>  
        @foreach ($questions as $question)
        <div class="row" id="question-{{ $question->id }}">
            {!! Form::model($question, [
                'method' => 'PATCH',
                'class' => 'form',
                'data-ajax-form' => true,
                'route' => [
                    'questions.update', $question->id
                ]
            ]) !!}
            <div class="col-sm-12">
                <div id="form-errors"></div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    {!! Form::text('question', null, ['class' => 'form-control', 'placeholder' => 'Enter Question']) !!}
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6">
                <div class="form-group">
                {!! Form::select('day_of_week', \App\Question::daysOptions(), null, ['class' => 'form-control', 'placeholder' => 'Select Day...']) !!}
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6">
                <div class="form-group">
                {!! Form::label('is_active') !!}
                {!! Form::checkbox('is_active', true, ['class' => 'form-control']) !!}
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                {!! Form::submit('Save', ['class' => 'btn btn-primary col-sm-12 col-md-6']) !!}
                {!! Form::button('Delete', [
                    'class' => 'btn btn-danger col-sm-12 col-md-6', 
                    'data-ajax-delete' => true,
                    'data-url' => route('questions.destroy', $question->id),
                    'data-target' => '#question-'.$question->id,
                    'data-confirm' => 'Are you sure you want to delete this question?'
                ]) !!}
                </div>
            </div>
            <br>
            {!! Form::close() !!}
        </div>
        <br>
        @endforeach
@endif
</div>