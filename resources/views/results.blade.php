@extends('layouts.app')

@section('content')
    @if ($answers->count())
    <h2>Hey {{ \Auth::user()->name }}</h2>
    <p>Here are the questions you answered previously</p>
    <div class="list-group">
        @foreach ($answers as $idx=>$answer)
        <a href="#" class="list-group-item list-group-item-info">
            <div class="media-left">
                <span class="media-object" style="text-align: center; font-weight: bold; width: 64px; height: 64px">{{++$idx}}</span>
            </div>
            <div class="media-body">
                <h4 class="media-heading">On {{$answer->question->day_of_week}} we asked you: {{$answer->question->question}}</h4>
                <div class="media well">
                    <div class="media-body">
                        <h4 class="media-heading">You answered:</h4>
                        <p>{{$answer->choice->value}}</p>
                    </div>
                </div>
            </div>
        </a>
        @endforeach
    </div>
    @else
    <div class="alert alert-info">
        <h2>Hey {{ \Auth::user()->name }}</h2>
        <p>You haven't answered any questions yet!</p>
        <a href="/">Go to your dashboard to answer today's questions</a>
    </div>
    @endif
    {!! $answers->links() !!}
@endsection

