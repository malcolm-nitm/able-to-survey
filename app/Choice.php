<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Choice extends Model
{
    public $fillable = ['survey_id', 'question_id', 'value'];

    public $rules = [
        'value' => 'required|alpha_dash',
        'question_id' => 'required|integer'
    ];

    /**
     * Setup event listeners on the model
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model)
        {
            if($model->question_id && !$model->survey_id) {
                $model->load('question');
                $model->survey_id = $model->question->survey_id;
            }
        });
    }

    /**
     * Return the survey for this question
     *
     * @return Illuminate\Database\Eloquent\Builder
     */
    public function survey() {
        return $this->belongsTo('App\Survey');
    }

    /**
     * Return the survey for this question
     *
     * @return Illuminate\Database\Eloquent\Builder
     */
    public function question() {
        return $this->belongsTo('App\Question');
    }

    /**
     * Return the user's answers
     *
     * @return Illuminate\Database\Eloquent\Builder
     */
    public function answers()
    {
        return $this->hasMany('App\Answer');
    }

    /**
     * Get the answer count
     *
     * @return Illuminate\Database\Eloquent\Builder
     */
    public function answerCount()
    {
        return $this->answers()
            ->selectRaw('question_id, count(*) as count')
            ->groupBy('question_id');
    }
}
