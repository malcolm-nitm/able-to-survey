<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Survey extends Model
{
    public $fillable = ['title', 'description', 'status', 'user_id'];

    public $rules = [
        'title' => 'required|string',
        'description' => 'required|string'
    ];
 
    /**
     * Setup event listeners on the model
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model)
        {
            $model->user_id = $model->user_id ?: \Auth::user()->id;
        });

        static::saved(function ($model)
        {
            if($model->is_active) {
                Survey::whereNot('id', $model->id)
                ->update(['is_active' => false]);
            }
        });
    }   
    /**
     * Return the user this survey belongs to
     *
     * @return Illuminate\Database\Eloquent\Builder
     */
    public function user() {
        return $this->belongsTo('App\User');
    }

    /**
     * Get questions for this survey
     *
     * @return Illuminate\Database\Eloquent\Builder
     */
    public function questions () {
        return $this->hasMany('App\Question');
    }

    /**
     * Get the question count
     *
     * @return Illuminate\Database\Eloquent\Builder
     */
    public function questionCount()
    {
        return $this->questions()
            ->selectRaw('survey_id, count(*) as count')
            ->groupBy('survey_id');
    }

    /**
     * Get answers for this survey
     *
     * @return Illuminate\Database\Eloquent\Builder
     */
    public function answers() {
        return $this->hasMany('App\Answer');
    }

    /**
     * Get the answer count
     *
     * @return Illuminate\Database\Eloquent\Builder
     */
    public function answerCount()
    {
        return $this->answers()
            ->selectRaw('survey_id, count(*) as count')
            ->groupBy('survey_id');
    }
}
