<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
    public $rules = [
        'choice_id' => 'required|integer|unique:answers,question_id,user_id',
        'question_id' => 'requried|integer'
    ];

    public $fillable = ['survey_id', 'question_id', 'choice_id', 'value'];

    public $messages = [
        'choice_id.unique' => "You've already answered this question!"
    ];

    /**
     * Setup event listeners on the model
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model)
        {
            $model->user_id = $model->user_id ?: \Auth::user()->id;
            if($model->choice_id && !$model->question_id) {
                $model->load('choice');
                $model->question_id = $model->choice->question_id;
            }
            if($model->question_id && !$model->survey_id) {
                $model->load('question');
                $model->survey_id = $model->question->survey_id;
            }
        });
    }

    /**
     * Return the survey for this answer
     *
     * @return Illuminate\Database\Eloquent\Builder
     */
    public function survey() {
        return $this->belongsTo('App\Survey');
    }

    /**
     * Return the queston for this answer
     *
     * @return Illuminate\Database\Eloquent\Builder
     */
    public function question() {
        return $this->belongsTo('App\Question');
    }

    /**
     * Return the choice for this answer
     *
     * @return Illuminate\Database\Eloquent\Builder
     */
    public function choice() {
        return $this->belongsTo('App\Choice');
    }
    
    /**
     * Return the user this answer belongs to
     *
     * @return Illuminate\Database\Eloquent\Builder
     */
    public function user() {
        return $this->belongsTo('App\User');
    }
}
