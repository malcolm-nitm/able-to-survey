<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    public $fillable = ['survey_id', 'title', 'question', 'day_of_week', 'is_active'];

    public $rules = [
        'question' => 'required|string',
        'day_of_week' => 'required|string',
        'survey_id' => 'required|integer|exists:surveys,id'
    ];

    /**
     * Setup event listeners on the model
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model)
        {
            $model->title = $model->title ?: substr($model->question, 0, 128);
        });
    }

    /**
     * Return the days array
     *
     * @return void
     */
    public static function daysOptions() {
        $days = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
        return array_combine($days, $days);
    }

    /**
     * Return the survey for this question
     *
     * @return Illuminate\Database\Eloquent\Builder
     */
    public function survey() {
        return $this->belongsTo('App\Survey');
    }

    /**
     * Return the user's answers
     *
     * @return Illuminate\Database\Eloquent\Builder
     */
    public function answers()
    {
        return $this->hasMany('App\Answer');
    }

    /**
     * Get the answer count
     *
     * @return Illuminate\Database\Eloquent\Builder
     */
    public function answerCount()
    {
        return $this->answers()
            ->selectRaw('question_id, count(*) as count')
            ->groupBy('question_id');
    }

    /**
     * Return the user's choices
     *
     * @return Illuminate\Database\Eloquent\Builder
     */
    public function choices()
    {
        return $this->hasMany('App\Choice');
    }

    /**
     * Get the choice count
     *
     * @return Illuminate\Database\Eloquent\Builder
     */
    public function choiceCount()
    {
        return $this->choices()
            ->selectRaw('question_id, count(*) as count')
            ->groupBy('question_id');
    }

    /**
     * Return only questions for which the user has not answered
     *
     * @param [type] $query
     * @return void
     */
    public function scopeAndUserHasNotAnswered($query, $day=null) {
        $day = $day ?: date('D');
        $query->whereNotIn('id', function ($query) use($day) {
            $questionsForToday = Question::select('id')->where(['day_of_week' => $day])->get()->pluck('id');
            $query->from((new Answer)->getTable())
                ->select('question_id')
                ->where(['user_id' => \Auth::user()->id])
                ->whereIn("question_id",  $questionsForToday->toArray());
        })
        ->where(['day_of_week' => $day]);
        // echo $query->toSql();
        // exit;
    }
}
