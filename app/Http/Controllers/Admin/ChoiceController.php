<?php

namespace App\Http\Controllers\Admin;

use App\Question;
use App\Survey;
use App\Choice;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;

class ChoiceController extends Controller
{
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index()
    {
        $models = Choice::latest()->with(['question', 'answerCount'])->orderBy('id', 'desc');
        if(request()->ajax()) {
            $data = request()->all();
            $depends = array_get($data, 'depends', []);
            if(count($depends)) {
                $models->where([
                    $depends['key'] => $depends['value']
                ]);
            }
            return response()->json($models->pluck('question', 'id'));
        }else {
            $models = $models->paginate(10);
            return view('admin.choices.index', compact('models'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
        }
    }
    
    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create()
    {
        $model = new Choice;
        return view('admin.choices.create', compact('model'));
    }
    
    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request)
    {
        request()->validate([
            'question_id' => 'required',
            'value' => 'required'
        ]);
        $id = $request->get('question_id');
        if ($id) {
            // Does the survey exits?
            $model = Question::find($id);
            if($model !== null) {
                $choice = $model->choices()->create($request->all());
                if ($request->ajax()) {
                    return response()->json([
                        'id' => $id,
                        'success' => 'Updated Question: '.$id.'!',
                        'html' => view('admin.choices.dynamic', ['model' => $choice])->render()
                    ]);
                }
                //If this wasn't an ajax request then send the user back to the survey
                return redirect()->route('questions.edit', ['id' => $id])
                ->with('success', 'Added new question');
            } else {
                abort(404, 'Survey not found: '.$id);
            }
        } else {
            return redirect()->route('questions.update', ['id' => $model->id])
            ->with('success', 'Saved new Question');
        }
    }
    
    /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function show($id)
    {
        $model = Choice::find($id);
        if($model) {
            return view('admin.choices.show', compact('model'));
        } else {
            abort(404, 'Choice not found: '.$id);
        }
    }
    
    /**
    * Show the form for editing the specified resource.
    *
    * @param  int $id
    * @return \Illuminate\Http\Response
    */
    public function edit($id)
    {
        $choice = Choice::find($id)->with('qustion');
        if($choice) {
            $model = $choice->question;
            return view('admin.choices.edit', compact('model'));
        } else {
            abort(404, 'Choice not found: '.$id);
        }
    }
    
    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request, $id)
    {
        request()->validate([
            'value' => 'required'
        ]);
        
        $model = Choice::where(['id' => $id])->first();
        if($model) {
            $model->fill($request->all());
            $model->save();
            if ($request->ajax()) {
                return response()->json([
                    'id' => $id,
            'question' => 'required',
                    'success' => 'Updated Choice: '.$id.'!'
                ]);
            } else {
                return redirect()->route('questions.edit', ['id' => $id])
                ->with('success', 'Updated Choice: '.$id.'!');
            }
        } else {
            abort(404, "Couldn't find choice: ".$id);
        }
    }
    
    /**
    * Remove the specified resource from storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function destroy(Request $request, $id)
    {
        $question = Choice::find($id);
        if($question) {
            $question->delete();
            if($request->ajax()) {
                return response()->json([
                    'id' => $id,
                    'success' => 'Deleted Choice: '.$id.'!'
                ]);
            } else {
                return redirect()->route('questions.index')
                ->with('success', 'Successfully deleted choice: '.$id);
            }
        } else {
            abort(404, 'Choice not found: '.$id);
        }
    }
}