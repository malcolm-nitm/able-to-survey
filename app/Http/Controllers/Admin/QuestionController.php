<?php

namespace App\Http\Controllers\Admin;

use App\Question;
use App\Survey;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;

class QuestionController extends Controller
{
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index()
    {
        $models = Question::latest()->with(['survey', 'answerCount'])->orderBy('id', 'desc');
        if(request()->ajax()) {
            $data = request()->all();
            $depends = array_get($data, 'depends', []);
            if(count($depends)) {
                $models->where([
                    $depends['key'] => $depends['value']
                ]);
            }
            return response()->json($models->pluck('question', 'id'));
        }else {
            $models = $models->paginate(10);
            return view('admin.questions.index', compact('models'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
        }
    }
    
    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create()
    {
        $model = new Question;
        return view('admin.questions.create', compact('model'));
    }
    
    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request)
    {
        request()->validate([
            'question' => 'required',
            'day_of_week' => 'required'
        ]);
        $id = $request->get('survey_id');
        if ($id) {
            // Does the survey exits?
            $model = Survey::find($id);
            if($model !== null) {
                $question = $model->questions()->create($request->all());
                if ($request->ajax()) {
                    return response()->json([
                        'id' => $id,
                        'success' => 'Updated Question: '.$id.'!',
                        'html' => view('admin.questions.dynamic', ['model' => $question])->render()
                    ]);
                }
                //If this wasn't an ajax request then send the user back to the survey
                return redirect()->route('questions.edit', ['id' => $question->id])
                ->with('success', 'Added new question');
            } else {
                abort(404, 'Survey not found: '.$id);
            }
        } else {
            return redirect()->route('questions.update', ['id' => $model->id])
            ->with('success', 'Saved new Question');
        }
    }
    
    /**
    * Display the specified resource.
    *
    * @param  \App\Question  $question
    * @return \Illuminate\Http\Response
    */
    public function show($id)
    {
        $model = Question::find($id);
        if($model) {
            return view('admin.questions.show', compact('model'));
        } else {
            abort(404, 'Question not found: '.$id);
        }
    }
    
    /**
    * Show the form for editing the specified resource.
    *
    * @param  int $id
    * @return \Illuminate\Http\Response
    */
    public function edit($id)
    {
        $model = Question::find($id);
        if($model) {
            return view('admin.questions.edit', compact('model'));
        } else {
            abort(404, 'Question not found: '.$id);
        }
    }
    
    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request, $id)
    {
        request()->validate([
            'question' => 'required',
            'day_of_week' => 'required'
        ]);
        
        $model = Question::where(['id' => $id])->with('choices')->first();
        if($model) {
            $model->fill($request->all());
            $model->save();
            if ($request->ajax()) {
                return response()->json([
                    'id' => $id,
                    'success' => 'Updated Question: '.$id.'!'
                ]);
            } else {
                return redirect()->route('questions.edit', ['id' => $id])
                ->with('success', 'Updated Question: '.$id.'!');
            }
        } else {
            abort(404, "Couldn't find question: ".$id);
        }
    }
    
    /**
    * Remove the specified resource from storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function destroy(Request $request, $id)
    {
        $question = Question::find($id);
        if($question) {
            $question->delete();
            if($request->ajax()) {
                return response()->json([
                    'id' => $id,
                    'success' => 'Deleted Question: '.$id.'!'
                ]);
            } else {
                return redirect()->route('questions.index')
                ->with('success', 'Successfully deleted question: '.$id);
            }
        } else {
            abort(404, 'Question not found: '.$id);
        }
    }
}