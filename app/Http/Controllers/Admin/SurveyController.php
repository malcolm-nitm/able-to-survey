<?php

namespace App\Http\Controllers\Admin;

use Auth;
use App\Survey;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;

class SurveyController extends Controller
{
    /**
    * Display a listing of the surveys.
    *
    * @return \Illuminate\Http\Response
    */
    public function index()
    {
        $models = Survey::latest()->with(['user', 'questionCount'])->orderBy('id', 'desc');
        if(request()->ajax()) {
            $data = request()->all();
            $depends = array_get($data, 'depends', []);
            if(count($depends)) {
                $models->where([
                    $depends['key'] => $depends['value']
                ]);
            }
            return response()->json($models->pluck('title', 'id'));
        }else {
            $models = $models->paginate(10);
            return view('admin.surveys.index', compact('models'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
        }
    }
    
    
    /**
    * Show the form for creating a new survey.
    *
    * @return \Illuminate\Http\Response
    */
    public function create()
    {
        $model = new Survey;
        return view('admin.surveys.create', compact('model'));
    }
    
    
    /**
    
    * Store a newly created surveys in storage.
    
    *
    
    * @param  \Illuminate\Http\Request  $request
    
    * @return \Illuminate\Http\Response
    
    */
    
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required|max:128',
            'description' => 'required|max:255',
            'status' => 'required',
        ]);
        
        $model = Auth::user()->surveys()->create($request->all());
        
        return redirect()->route('surveys.update', ['id' => $model->id])
        ->with('success', 'Saved new Survey');
    }
    
    
    /**
    * Display the specified surveys.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function show($id)
    {
        $model = Survey::find($id);
        if($model) {
            return view('admin.surveys.show', compact('model'));
        } else {
            abort(404, 'Survey not found: '.$id);
        }
    }
    
    
    /**
    * Show the form for editing the specified surveys.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function edit($id)
    {
        $model = Survey::find($id);
        if($model) {
            return view('admin.surveys.edit', compact('model'));
        } else {
            abort(404, 'Survey not found: '.$id);
        }
    }
    
    /**
    * Update the specified surveys in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request, $id)
    {
        request()->validate([
            'title' => 'required',
            'description' => 'required',
        ]);

        $model = Survey::where(['id' => $id])->first();
        if($model) {
            $model->fill($request->all());
            $model->save();
            if ($request->ajax()) {
                return response()->json([
                    'id' => $id,
                    'success' => 'Updated Survey: '.$id.'!'
                ]);
            } else {
                return redirect()->route('surveys.edit', ['id' => $id])
                ->with('success', 'Updated Survey: '.$id.'!');
            }
        } else {
            abort(404, "Couldn't find survey: ".$id);
        }
    }
    
    /**
    * Remove the specified surveys from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function destroy($id)
    {
        Survey::find($id)->delete();
        return redirect()->route('surveys.index')
        ->with('success', 'Successfully deleted survey: '.$id);
    }
}