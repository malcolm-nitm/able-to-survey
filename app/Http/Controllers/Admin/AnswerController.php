<?php

namespace App\Http\Controllers\Admin;

use App\Question;
use App\Answer;
use App\Survey;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;

class AnswerController extends Controller
{
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index()
    {
        $models = Answer::latest()->with(['survey', 'question', 'user', 'choice'])->orderBy('id', 'desc');
        if(request()->ajax()) {
            $data = request()->all();
            $depends = array_get($data, 'depends', []);
            if(count($depends)) {
                $models->where([
                    $depends['key'] => $depends['value']
                ]);
            }
            return response()->json($models->pluck('value', 'id'));
        }else {
            $models = $models->paginate(10);
            return view('admin.answers.index', compact('models'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
        }
    }
    
    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create()
    {
        $model = new Answer;
        return view('admin.answers.create', compact('model'));
    }
    
    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param int $id Optional Survey Id
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request)
    {
        request()->validate([
            'value' => 'required',
            'question_id' => 'required'
        ]);
        $id = $request->get('survey_id');
        $questionId = $request->get('question_id');
        if ($id) {
            // Does the survey exits?
            $question = Question::find($questionId);
            if($question) {
                $data = $request->all();
                $answer = $question->answers()->create($data);
                if ($request->ajax()) {
                    return response()->json([
                        'id' => $id,
                        'success' => 'Updated Answer: '.$id.'!',
                        'html' => view('admin.answers.dynamic', ['model' => $answer])->render()
                    ]);
                }
                //If this wasn't an ajax request then send the user back to the survey
                return redirect()->route('answers.edit', ['id' => $answer->id])
                ->with('success', 'Added new answer');
            } else {
                abort(404, 'Question not found: '.$id);
            }
        } else {
            return redirect()->route('answers.update', ['id' => $model->id])
            ->with('success', 'Saved new Question');
        }
    }
    
    /**
    * Display the specified resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function show($id)
    {
        $model = Answer::find($id);
        if($model) {
            return view('admin.answers.show', compact('model'));
        } else {
            abort(404, 'Answer not found: '.$id);
        }
    }
    
    /**
    * Show the form for editing the specified resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function edit($id)
    {
        $model = Answer::find($id);
        if($model) {
            return view('admin.answers.edit', compact('model'));
        } else {
            abort(404, 'Answer not found: '.$id);
        }
    }
    
    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request, $id)
    {
        request()->validate([
            'answer' => 'required'
        ]);
        
        $model = Answer::where(['id' => $id])->first();
        if($model) {
            $model->fill($request->all());
            $model->save();
            if ($request->ajax()) {
                return response()->json([
                    'id' => $id,
                    'success' => 'Updated Answer: '.$id.'!'
                ]);
            } else {
                return redirect()->route('answers.edit', ['id' => $id])
                ->with('success', 'Updated Answer: '.$id.'!');
            }
        } else {
            abort(404, "Couldn't find answer: ".$id);
        }
    }
    
    /**
    * Remove the specified resource from storage.
    *
    * @return \Illuminate\Http\Response
    */
    public function destroy(Request $request, $id)
    {
        $answer = Answer::find($id);
        if($answer) {
            $answer->delete();
            if($request->ajax()) {
                return response()->json([
                    'id' => $id,
                    'success' => 'Deleted Answer: '.$id.'!'
                ]);
            } else {
                return redirect()->route('answer.index')
                ->with('success', 'Successfully deleted answer: '.$id);
            }
        } else {
            abort(404, 'Answer not found: '.$id);
        }
    }
}