<?php

namespace App\Http\Controllers;

use App\Question;
use App\Answer;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class HomeController extends Controller
{
    /**
    * Create a new controller instance.
    *
    * @return void
    */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
    * Show the application dashboard.
    *
    * @return \Illuminate\Http\Response
    */
    public function index()
    {
        // $survey = Survey::where(['is_active' => true])->orderBy('id', 'desc')->first();
        $query = Question::latest()
        ->with(['choices', 'survey'])
        ->where(['is_active' => true])
        ->andUserHasNotAnswered(date('D'))
        ->limit(2);
        
        // echo $query->toSql();
        // print_r($query->getBindings());
        // exit;
        $questions = $query->get();
        
        return view('home', [
        'question' => $questions->shift(),
        'next' => $questions->shift()
        ]);
    }
    
    /**
    * Show the users results.
    *
    * @return \Illuminate\Http\Response
    */
    public function results()
    {
        $query = \Auth::user()->answers()->latest()
        ->with(['choice', 'survey'])
        ->limit(2);
        
        $answers = $query->paginate(10);
        
        return view('results', [
        'answers' => $answers,
        ]);
    }
    
    /**
    * Store the user's answers
    *
    * @param [type] $request
    * @return void
    */
    public function answer() {
        request()->validate([
        'question_id' => 'required|integer',
        'choice_id' => [
        'required',
        'integer',
        Rule::unique('answers')->where(function($query) {
            return $query->where('user_id', \Auth::user()->id)
            ->where('question_id', request()->get('question_id'));
        })
        ],
        ], [
        'choice_id.unique' => "You've already answered this question!"
        ]);
        $id = request()->get('survey_id');
        $questionId = request()->get('question_id');
        if ($questionId) {
            // Does the survey exits?
            $question = Question::find($questionId);
            if($question) {
                $data = request()->all();
                $answer = \Auth::user()->answers()->create(array_merge([
                'question_id' => $question->id,
                'survey_id' => $question->survey_id
                ], $data));
            } else {
                abort(404, 'Question not found: '.$id);
            }
        } else {
            abort(404, 'No question specified: '.$questionId);
        }
        
        $args = [];
        $view = 'done';
        $query = Question::latest()
        ->with(['choices', 'survey'])
        ->where(['is_active' => true])
        ->where('id', '!=', $questionId)
        ->andUserHasNotAnswered(date('D'))
        ->limit(2);
        $questions = $query->get();
        if($questions->count()) {
            //If there are more questions then let's show it!
            $view = 'question';
            $args['question'] = $questions->shift();
            if($next = $questions->shift()) {
                $args['next'] = $next;
            }
        }
        
        return response()->json([
        'html' => view($view, $args)->render()
        ]);
    }
}