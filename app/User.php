<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Undocumented function
     *
     * @return boolean
     */
    public function isAdmin()
    {
        return $this->is_admin; // this looks for an admin column in your users table
    }

    /**
     * Return the user's surveys
     *
     * @return Builder
     */
    public function surveys()
    {
        return $this->hasMany('App\Survey');
    }

    /**
     * Get the survey count
     *
     * @return Illuminate\Database\Eloquent\Builder
     */
    public function surveyCount()
    {
        return $this->surveys()
            ->selectRaw('user_id, count(*) as count')
            ->groupBy('user_id');
    }

    /**
     * Return the user's questions
     *
     * @return Builder
     */
    public function questions()
    {
        return $this->hasMany('App\Question');
    }

    /**
     * Return the user's answers
     *
     * @return Builder
     */
    public function answers()
    {
        return $this->hasMany('App\Answer');
    }

    /**
     * Get the answer count
     *
     * @return Illuminate\Database\Eloquent\Builder
     */
    public function answerCount()
    {
        return $this->answers()
            ->selectRaw('user_id, count(*) as count')
            ->groupBy('user_id');
    }
}
