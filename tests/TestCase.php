<?php

namespace Tests;

use Mail;
use Artisan;
use App\User;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    /**
     * Default preparation for each test
     *
     */
    public function setUp()
    {
        parent::setUp();
        $this->prepareForTests();
    }

    /**
     * Migrates the database.
     * This will cause the tests to run quickly.
     *
     */
    private function prepareForTests()
    {
        Artisan::call('migrate');
        Artisan::call('db:seed');
        // Mail::pretend(true);
        //Let's login the user
        $this->setupAuth();
    }

    /**
     * Setup the authenticated user
     *
     * @return void
     */
    protected function setupAuth() {
        $user = $this->createUser();
        $this->be($user);
        \Auth::setUser($user);
    }
    

    /**
     * Create a dummy user model
     *
     * @return App\User
     */
    protected function createUser() {
        $model = User::first();
        return $model;
    }
    

    /**
     * Create a dummy survey model
     *
     * @return App\Survey
     */
    protected function createSurvey() {
        $user = $this->createUser();
        $model = $user->surveys()->create([
            'title' => 'Test',
            'description' => 'Test',
        ]);
        return $model;
    }

    /**
     * Create a dummy question model
     *
     * @param boolean $randomQuestion Generate a random question?
     * @return App\Question
     */
    protected function createQuestion($randomQuestion=false) {
        $survey = $this->createSurvey();
        $model = $survey->questions()->create([
            'title' => $randomQuestion ? str_random(12) : 'Test',
            'question' => $randomQuestion ? str_random(12) : 'Test',
            'day_of_week' => 'Sun'
        ]);
        return $model;
    }

    /**
     * Create a dummy choice model
     * @param boolean $random Generate a random choice?
     * @param boolean $randomQuestion Generate a random question?
     * @return App\Choice
     */
    protected function createChoice($random=true, $randomQuestion=false) {
        $question = $this->createQuestion($randomQuestion);
        $model = $question->choices()->create([
            'value' => $random ? str_random(12) : 'Test Value',
        ]);
        return $model;
    }

    /**
     * Create a dummy answer model
     *
     * @param boolean $random Generate a random choice?
     * @return App\Answer
     */
    protected function createAnswer($randomChoice=true) {
        $choice = $this->createChoice($randomChoice);
        $model = $choice->answers()->create([
            'user_id' => 1,
        ]);
        return $model;
    }
}
