<?php

namespace Tests\Unit;

use Validator;
use App\Question;
use App\Answer;
use App\Choice;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AnswerModelTest extends TestCase
{
    /**
     * Test validating answer model.
     *
     * @return void
     */
    public function test_validate_empty_answer_model()
    {
        $model = new Answer;
        
        $v = Validator::make([
        ], $model->rules);

        $this->assertTrue($v->fails());
    }

    /**
     * Test validating answer model.
     *
     * @return void
     */
    public function test_validate_answer_model()
    {
        $model = new Answer;
        
        $choice = $this->createChoice();
        $v = Validator::make([
            'choice_id' => $choice->id,
        ], $model->rules);

        $this->assertTrue($v->passes());
    }

    /**
     * Test validating answer model.
     *
     * @return void
     */
    public function test_validate_can_create_answer_model()
    {
        $model = new Answer;

        //We're creating a non random model to be reused in next test
        $choice = $this->createChoice(false);
        $data = [
            'choice_id' => $choice->id,
        ];
        $v = Validator::make($data, $model->rules);

        $model->fill($data);
        $model->save();

        $this->assertTrue($model->exists);
    }

    /**
     * Test validating answer model.
     *
     * @return void
     */
    public function test_validate_unique_answer_model()
    {
        $model = new Answer;

        $choice = $this->createChoice(false);
        
        $data = [
            'choice_id' => $choice->id,
        ];
        $this->expectException('PDOException', 'SQLSTATE[23000]: Integrity constraint violation: 19 UNIQUE constraint failed: answers.user_id, answers.question_id');
        $model->fill($data);
        $model->save();

        //Save the model again
        $model = new Answer;
        $model->fill($data);
        $model->save();
    }

    /**
     * Test validating answer model.
     *
     * @return void
     */
    public function test_validate_can_delete_answer_model()
    {
        $model = new Answer;
        
        $choice = $this->createChoice();
        $data = [
            'choice_id' => $choice->id,
        ];
        $v = Validator::make($data, $model->rules);

        $model->fill($data);
        $model->save();
        $model->delete();

        $this->assertTrue(!$model->exists);
    }

    /**
     * Test validating answer model.
     *
     * @return void
     */
    public function test_validate_adding_answers_to_choice_model()
    {
        $model = new Answer;
        
        $choice = $this->createChoice(true, true);

        $choice->answers()->create([
            'choice_id' => $choice->id,
        ]);

        $this->assertTrue($choice->answers()->count() > 0);
    }
}
