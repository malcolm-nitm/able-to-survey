<?php

namespace Tests\Unit;

use Validator;
use App\Survey;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class SurveyModelTest extends TestCase
{
    /**
     * Test validating survey model.
     *
     * @return void
     */
    public function test_validate_empty_survey_model()
    {
        $model = new Survey;
        
        $v = Validator::make([
        ], $model->rules);

        $this->assertTrue($v->fails());
    }

    /**
     * Test validating survey model.
     *
     * @return void
     */
    public function test_validate_partial_survey_model()
    {
        $model = new Survey;
        
        $v = Validator::make([
            'title' => 'Test'
        ], $model->rules);

        $this->assertTrue($v->fails());
    }

    /**
     * Test validating survey model.
     *
     * @return void
     */
    public function test_validate_survey_model()
    {
        $model = new Survey;
        $v = Validator::make([
            'title' => 'Test',
            'description' => 'Test',
        ], $model->rules);

        $this->assertTrue($v->passes());
    }

    /**
     * Test validating survey model.
     *
     * @return void
     */
    public function test_validate_can_create_survey_model()
    {
        $model = new Survey;
        
        $data = [
            'title' => 'Test',
            'description' => 'Test',
            'user_id' => 1
        ];
        $v = Validator::make($data, $model->rules);

        $model->fill($data);
        $model->save();

        $this->assertTrue($model->exists);
    }

    /**
     * Test validating survey model.
     *
     * @return void
     */
    public function test_validate_can_update_survey_model()
    {
        $model = new Survey;
        
        $data = [
            'title' => 'Test',
            'description' => 'Test',
            'user_id' => 1,
            'is_active' => true
        ];
        $v = Validator::make($data, $model->rules);

        $model->fill($data);
        $model->save();

        $oldAttributes = [
            'title' => $model->title,
            'is_active' => $model->is_active
        ];

        $newAttributes = [
            'title' => 'New Title',
            'is_active' => false
        ];

        $model->fill($newAttributes);
        $model->save();

        $this->assertEquals([
            'title' => $model->title,
            'is_active' => $model->isActive
        ],  $newAttributes);
    }

    /**
     * Test validating survey model.
     *
     * @return void
     */
    public function test_validate_can_delete_survey_model()
    {
        $model = new Survey;
        
        $data = [
            'title' => 'Test',
            'description' => 'Test',
            'user_id' => 1
        ];
        $v = Validator::make($data, $model->rules);

        $model->fill($data);
        $model->save();
        $model->delete();

        $this->assertTrue(!$model->exists);
    }

    /**
     * Test validating survey model.
     *
     * @return void
     */
    public function test_validate_adding_questions_to_survey_model()
    {
        $model = new Survey;
        
        $data = [
            'title' => 'Test',
            'description' => 'Test',
            'user_id' => 1
        ];
        $v = Validator::make($data, $model->rules);

        $model->fill($data);
        $model->save();

        $model->questions()->create([
            'question' => 'Does this work?',
            'day_of_week' => 'Mon'
        ]);

        $this->assertTrue($model->questions()->count() == 1);
    }
}
