<?php

namespace Tests\Unit;

use Validator;
use App\Question;
use App\Survey;
use App\Choice;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class QuestionModelTest extends TestCase
{
    /**
     * Test validating question model.
     *
     * @return void
     */
    public function test_validate_empty_question_model()
    {
        $model = new Question;
        
        $v = Validator::make([
        ], $model->rules);

        $this->assertTrue($v->fails());
    }

    /**
     * Test validating question model.
     *
     * @return void
     */
    public function test_validate_partial_question_model()
    {
        $model = new Question;
        
        $v = Validator::make([
            'question' => 'Test'
        ], $model->rules);

        $this->assertTrue($v->fails());
    }

    /**
     * Test validating question model.
     *
     * @return void
     */
    public function test_validate_question_model()
    {
        $model = new Question;
        
        $survey = $this->createSurvey();
        $v = Validator::make([
            'question' => 'Test',
            'title' => 'Test',
            'survey_id' => $survey->id,
            'day_of_week' => 'Mon'
        ], $model->rules);

        $this->assertTrue($v->passes());
    }

    /**
     * Test validating question model.
     *
     * @return void
     */
    public function test_validate_can_create_question_model()
    {
        $model = new Question;

        $survey = $this->createSurvey();
        $data = [
            'question' => 'Test',
            'title' => 'Test',
            'survey_id' => $survey->id,
            'day_of_week' => 'Mon'
        ];
        $v = Validator::make($data, $model->rules);

        $model->fill($data);
        $model->save();

        $this->assertTrue($model->exists);
    }

    /**
     * Test validating question model.
     *
     * @return void
     */
    public function test_validate_can_update_question_model()
    {
        $model = new Question;
        $survey = $this->createSurvey();

        $data = [
            'question' => 'Test',
            'title' => 'Test',
            'survey_id' => $survey->id,
            'is_active' => true,
            'day_of_week' => 'Mon'
        ];
        $v = Validator::make($data, $model->rules);

        $model->fill($data);
        $model->save();

        $oldAttributes = [
            'question' => $model->question,
            'is_active' => $model->is_active
        ];

        $newAttributes = [
            'question' => 'New Title',
            'is_active' => false
        ];

        $model->fill($newAttributes);
        $model->save();

        $this->assertEquals([
            'question' => $model->question,
            'is_active' => $model->isActive
        ],  $newAttributes);
    }

    /**
     * Test validating question model.
     *
     * @return void
     */
    public function test_validate_can_delete_question_model()
    {
        $model = new Question;
        
        $survey = $this->createSurvey();
        $data = [
            'question' => 'Test',
            'title' => 'Test',
            'survey_id' => $survey->id,
            'day_of_week' => 'Mon'
        ];
        $v = Validator::make($data, $model->rules);

        $model->fill($data);
        $model->save();
        $model->delete();

        $this->assertTrue(!$model->exists);
    }

    /**
     * Test validating question model.
     *
     * @return void
     */
    public function test_validate_adding_choices_to_question_model()
    {
        $model = new Question;
        
        $survey = $this->createSurvey();
        $data = [
            'question' => 'Test',
            'title' => 'Test',
            'survey_id' => $survey->id,
            'day_of_week' => 'Mon'
        ];
        $v = Validator::make($data, $model->rules);

        $model->fill($data);
        $model->save();

        $model->choices()->create([
            'value' => 'Does this work?',
            'survey_id' => $survey->id,
            'question_id' => $model->id
        ]);

        $this->assertTrue($model->choices()->count() == 1);
    }
}
