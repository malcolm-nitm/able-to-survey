<?php

namespace Tests\Unit;

use Validator;
use App\Question;
use App\Survey;
use App\Choice;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ChoiceModelTest extends TestCase
{
    /**
     * Test validating question model.
     *
     * @return void
     */
    public function test_validate_empty_choice_model()
    {
        $model = new Choice;
        
        $v = Validator::make([
        ], $model->rules);

        $this->assertTrue($v->fails());
    }

    /**
     * Test validating question model.
     *
     * @return void
     */
    public function test_validate_partial_choice_model()
    {
        $model = new Choice;
        
        $v = Validator::make([
            'value' =>  'Test'
        ], $model->rules);

        $this->assertTrue($v->fails());
    }

    /**
     * Test validating question model.
     *
     * @return void
     */
    public function test_validate_choice_model()
    {
        $model = new Choice;
        
        $question = $this->createQuestion();
        $v = Validator::make([
            'value' =>  'Test',
            'question_id' => $question->id,
        ], $model->rules);

        $this->assertTrue($v->passes());
    }

    /**
     * Test validating question model.
     *
     * @return void
     */
    public function test_validate_can_create_choice_model()
    {
        $model = new Choice;

        $question = $this->createQuestion();
        $data = [
            'value' =>  'Test',
            'question_id' => $question->id,
        ];
        $v = Validator::make($data, $model->rules);

        $model->fill($data);
        $model->save();

        $this->assertTrue($model->exists);
    }

    /**
     * Test validating question model.
     *
     * @return void
     */
    public function test_validate_can_update_choice_model()
    {
        $model = new Choice;
        $question = $this->createQuestion();

        $data = [
            'value' =>  'Test',
            'question_id' => $question->id,
        ];
        $v = Validator::make($data, $model->rules);

        $model->fill($data);
        $model->save();

        $oldAttributes = [
            'value' =>  $model->value,
            'is_active' => $model->is_active
        ];

        $newAttributes = [
            'value' =>  'New Value',
        ];

        $model->fill($newAttributes);
        $model->save();

        $this->assertEquals([
            'value' =>  $model->value,
        ],  $newAttributes);
    }

    /**
     * Test validating question model.
     *
     * @return void
     */
    public function test_validate_can_delete_choice_model()
    {
        $model = new Choice;
        
        $question = $this->createQuestion();
        $data = [
            'value' =>  'Test',
            'question_id' => $question->id,
        ];
        $v = Validator::make($data, $model->rules);

        $model->fill($data);
        $model->save();
        $model->delete();

        $this->assertTrue(!$model->exists);
    }

    /**
     * Test validating question model.
     *
     * @return void
     */
    public function test_validate_adding_choices_to_choice_model()
    {
        $model = new Choice;
        
        $question = $this->createQuestion();
        $data = [
            'value' =>  'Test',
            'question_id' => $question->id,
        ];
        $v = Validator::make($data, $model->rules);

        $model->fill($data);
        $model->save();

        $question->choices()->create([
            'value' => 'Does this work?',
            'question_id' => $question->id,
        ]);

        $this->assertTrue($question->choices()->count() > 0);
    }
}
